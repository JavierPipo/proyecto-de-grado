import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// componentes
import { MaladyListComponent } from '../components/maladies/malady-list/malady-list.component';
import { MapComponent } from '../components/map/map.component';
import { ErrorRouteComponent } from '../components/error-route/error-route.component';
import { MaladyDetailComponent } from '../components/maladies/malady-detail/malady-detail.component';
import { HomeComponent } from '../components/home/home.component';
import { AcercaComponent } from '../components/acerca/acerca.component';
import { LoginComponent } from '../components/login/login.component';
import { RealizarAporteComponent } from '../components/aporte/realizar/realizar-aporte.component';
import { MainComponent } from '../components/main/main.component';
import { AporteComponent } from '../components/aporte/mis aportes/aporte.component';
import { DetailAporteComponent } from '../components/aporte/detail/detail-aporte.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'detail-aportes/:id', component: DetailAporteComponent},
  {path: 'my-aportes/:id', component: AporteComponent},
  {path: 'home', component: HomeComponent},
  {path: 'main', component: MainComponent},
  {path: 'maladies', component: MaladyListComponent},
  {path: 'acerca', component: AcercaComponent},
  {path: 'map', component: MapComponent},
  {path: 'login', component: LoginComponent},
  {path: 'malady-detail/:id', component: MaladyDetailComponent},
  {path: 'realizar-aporte', component: RealizarAporteComponent},
  {path: '**', component: ErrorRouteComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
