import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaladyListComponent } from './malady-list.component';

describe('MaladyListComponent', () => {
  let component: MaladyListComponent;
  let fixture: ComponentFixture<MaladyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaladyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaladyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
