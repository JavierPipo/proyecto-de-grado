import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MaladiesService } from '../../../services/malady/maladies.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthServices } from '../../../services/auth/auth.service';
import { Global } from '../../../global';

@Component({
  selector: 'malady-list',
  templateUrl: './malady-list.component.html',
  styleUrls: ['./malady-list.component.css'],
  providers: [ MaladiesService, AuthServices ]
})
export class MaladyListComponent implements OnInit {
  public objMaladies: object;
  public url: string;

  constructor(private _router: Router,
              private _maladiesService: MaladiesService,
              private _authServices: AuthServices,
              private _glbal: Global) {
    this.url = this._glbal.urlApi;
  }

  ngOnInit() {
    console.log('este es el token: ' + localStorage.getItem('Authorization'));
    this._getAllMaladies();
  }

  private _getAllMaladies() {
    this._maladiesService.getAllMaladies().subscribe(
      this._fnSuccessGetMaladies.bind(this),
      this._fnErrorGetMaladies.bind(this)
    );
  }

  fnGoToDetail(idMalady) {
    this._router.navigate(['malady-detail', idMalady]);
  }

  private _fnSuccessGetMaladies(result: any) {
    this.objMaladies = result['data'];
    console.log(this.objMaladies);
  }

  private _fnErrorGetMaladies(error: any) {
    console.log(error);
    let sedPost: boolean;
    sedPost = false;

    if (error instanceof HttpErrorResponse) {
      if (error.status == 401) {
        sedPost = true;
        console.log('si es 401');
        this._authServices.errorAuthorization();
      }
    }else {
      console.log('else de httpresponse');
    }

    if (sedPost) {
      this.ngOnInit();
    }
  }

}
