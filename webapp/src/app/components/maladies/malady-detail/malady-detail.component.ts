import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaladiesService } from '../../../services/malady/maladies.service';
import { Global } from '../../../global';

@Component({
  selector: 'malady-detail',
  templateUrl: './malady-detail.component.html',
  styleUrls: ['./malady-detail.component.css'],
  providers: [ MaladiesService ]
})

export class MaladyDetailComponent implements OnInit {
  private _idMalady: number;
  public objDataMalady: object;
  public objDataImages: object;
  public objDataSymptoms: object;
  public url: string;

  constructor(private _activateRoute: ActivatedRoute,
              private _router: Router,
              private _maladiesServices: MaladiesService,
              private _global: Global) {
    this.objDataMalady = {
      name: '',
      description: ''
    };
    this.url = this._global.urlApi;
  }

  ngOnInit() {
    this._idMalady = this._activateRoute.snapshot.params['id'];
    this._getDataMalady();
    this._getDataImages();
    this._getDataSymptoms();
  }

  private _getDataMalady() {
    this._maladiesServices.getOneMalady(this._idMalady).subscribe(
      this._successGetMalady.bind(this),
      this._errorGetMalady.bind(this));
  }
  private _successGetMalady(resul: any) {
    this.objDataMalady = resul['data'];
  }
  private _errorGetMalady(error: any) {
    console.log('error al traer las enfermedades: ' + error);
  }

  private _getDataImages() {
    this._maladiesServices.getAllImagesByMaladyId(this._idMalady).subscribe(
      this._successGetImages.bind(this),
      this._errorGetImages.bind(this)
    );
  }
  private _successGetImages(result: any) {
    this.objDataImages = result['data'];
  }
  private _errorGetImages(error: any) {
    console.log('error al traer todas las imagenes: ' + error);
  }

  private _getDataSymptoms() {
    this._maladiesServices.getAllSymptomsByMaladyId(this._idMalady).subscribe(
      this._successGetSymptoms.bind(this),
      this._errorGetSymptoms.bind(this)
    );
  }
  private _successGetSymptoms(result: any) {
    this.objDataSymptoms = result['data'];
  }
  private _errorGetSymptoms(error: any) {
    console.log('error al traer todas los symptom: ' + error);
  }

}
