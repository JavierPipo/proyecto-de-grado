import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaladyDetailComponent } from './malady-detail.component';

describe('MaladyDetailComponent', () => {
  let component: MaladyDetailComponent;
  let fixture: ComponentFixture<MaladyDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaladyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaladyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
