import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { AporteService } from '../../../services/aporte/aporte.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aporte',
  templateUrl: './aporte.component.html',
  styleUrls: ['./aporte.component.css'],
  providers: [AporteService]
})

export class AporteComponent implements OnInit {


  private _idUser: number;
  data = [];
  interfaces: IDataInfo[];
  dataSource: any;
  displayedColumnstable = ['name', 'last', 'city', 'relationship'];

  constructor(private _activatedRoute: ActivatedRoute,
              private _aporteService: AporteService,
              private _router: Router) { }

  ngOnInit() {
    this._idUser = this._activatedRoute.snapshot.params['id'];
    this._fnGetAllMyAportes();
  }

  private _fnGetAllMyAportes() {
    this._aporteService.fnGetAllMyAportes(this._idUser).subscribe(
      this._successGetMyAportes.bind(this),
      this._errorGetMyAportes.bind(this)
    );
  }
  private _successGetMyAportes(result: any) {
    this._fnFillDataSource(result);
  }
  private _fnFillDataSource(objData: any) {

    for ( let i = 0 ; i < objData['data'].length; i++) {
      this.data.push(
        {
          id: objData['data'][i]['id'],
          name: objData['data'][i]['name_person'],
          last: objData['data'][i]['last_name_person'],
          city: objData['data'][i]['city']['name'],
          relationship: objData['data'][i]['relationship']['name']
        });
    }
    this.interfaces = this.data;

    this.dataSource = new MatTableDataSource(this.interfaces);
  }
  private _errorGetMyAportes(error: any) {
    console.log('error en : _errorGetMyAportes');
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  public goToDetailAporte(data) {
    let idAporte: string;
    idAporte = data['id'];
    this._router.navigate(['detail-aportes', idAporte ]);
  }


}

export interface IDataInfo {
  name: string;
  last: string;
  city: string;
  relationship: string;
  id: string;
}
