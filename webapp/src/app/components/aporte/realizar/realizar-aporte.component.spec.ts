import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealizarAporteComponent } from './realizar-aporte.component';

describe('RealizarAporteComponent', () => {
  let component: RealizarAporteComponent;
  let fixture: ComponentFixture<RealizarAporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealizarAporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealizarAporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
