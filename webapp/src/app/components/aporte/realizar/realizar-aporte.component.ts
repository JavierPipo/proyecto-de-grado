import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AporteService } from '../../../services/aporte/aporte.service';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
import { MapaService } from '../../../services/mapa/mapa.service';
import { UserService } from '../../../services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-realizar-aporte',
  templateUrl: './realizar-aporte.component.html',
  styleUrls: ['./realizar-aporte.component.css'],
  providers: [AporteService, MapaService, UserService]
})
export class RealizarAporteComponent implements OnInit {

  private _idUser: string;
  public listSymptoms: any;

  simptoms = [];

  filteredOptions: Observable<string[]>;

  selectable: boolean = true;
  removable: boolean = true;

  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  myControl: FormControl = new FormControl();

  public objAllrelationships: object;
  private objDataComplit: object;

  objNewUser = [
    {value: 'si', name: 'Si'},
    {value: 'no', name: 'No'}
  ];

  constructor(private _formBuilder: FormBuilder,
              private _aporteServices: AporteService,
              private _mapaService: MapaService,
              private _userService: UserService,
              private _router: Router) {
  }


  ngOnInit() {


    this.getAllrelationships();
    this._getAllSymptoms();

    this.firstFormGroup = this._formBuilder.group({
      relationsShip: ['', Validators.required],
      existUser: ['', Validators.required],
      idAportante: ''
    });
    this.secondFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      last_names: ['', Validators.required],
      date_born: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      arrSymptoms: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      localitation: ['', Validators.required]
    });

    this.objDataComplit = {
      name_person: '',
      last_name_person: '',
      date_born: '',
      relationship_id: '',
      loc_longitude: '',
      loc_latitude: '',
      arrSymptoms: []
    };
  }

  private _myCoordinates(result: any) {
    console.log(result.coords.latitude);
    console.log(result.coords.longitude);

    this.objDataComplit['loc_latitude'] = result.coords.latitude;
    this.objDataComplit['loc_longitude'] = result.coords.longitude;

    this._mapaService.getDepartmentAndCityByLongLati(
      this.objDataComplit['loc_latitude'],
      this.objDataComplit['loc_longitude']).subscribe(
        this._fnGetDepartmentCity.bind(this),
        this._fnErrorGetDepartmentCity.bind(this)
    );
  }

  private _fnGetDepartmentCity(result: any) {
    this.objDataComplit['city_id'] = result['results'][0].formatted_address;
    this.fourthFormGroup.controls.localitation.reset('YES');
  }
  private _fnErrorGetDepartmentCity(error: any) {
    console.log(error);
  }

  private _errorGetCoordinates(error: any) {
    console.log(error);
    if (error.code == 1) {
      console.log('el usuario denego la accion');
      console.log('chrome://settings/content/location');

    }else {
      console.log('otro error obteniendo la localizacion');//TODO cambiar este mensaje
    }
  }

  public fnavailableLocation() {
    console.log('click');
    navigator.geolocation.getCurrentPosition(
      this._myCoordinates.bind(this),
      this._errorGetCoordinates.bind(this));
  }
  public fnSendData() {
    this.objDataComplit['relationship_id'] = this.firstFormGroup.value.relationsShip;
    this.objDataComplit['name_person'] = this.secondFormGroup.value.name;
    this.objDataComplit['last_name_person'] = this.secondFormGroup.value.last_names;
    this.objDataComplit['date_born'] = this.secondFormGroup.value.date_born;
    this.objDataComplit['arrSymptoms'] = JSON.stringify(this.simptoms) ;

    this._userService.getMyInformation().subscribe(
      this._fnSendDataAporte.bind(this),
      this._fnErrorGetMyInformation.bind(this)
    );
  }
  private _fnSendDataAporte(result: any) {
    this._idUser = result['data']['id'];

    this._aporteServices.fnSaveAporte(this.objDataComplit, this._idUser).subscribe(
      this._fnSuccessSaveAporte.bind(this),
      this._fnErrorSaveAporte.bind(this)
    );
  }

  private _fnSuccessSaveAporte(result: any) {
    this._router.navigate(['my-aportes', this._idUser]);
    console.log(result);
  }

  private _fnErrorSaveAporte(error: any) {
    console.log('Error guardando el aporte');
    console.log(error);
  }

  private _fnErrorGetMyInformation(error: any) {
    console.log('Error obteniendo la informaion del usuario...');
    console.log(error);
  }

  cleanInputSimptom() {
    console.log('limpiar caja');
    this.myControl.reset('');
  }

  public fnInitFilter() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }

  public fnExistAporteForUser() {
    console.log(this.firstFormGroup.value.existUser);
  }

  clickOnSymptom() {
    let arrOption: any;
    arrOption = this.myControl.value.split('?');
    this.myControl.reset(arrOption[1]);

    this.simptoms.push({"name": arrOption[1], "id": arrOption[0]})

    this.thirdFormGroup.controls.arrSymptoms.reset('yes');
    console.log('paso a agregar el YES');

  }

  public fnValidateSymptom() {
    console.log(this.thirdFormGroup.controls.arrSymptoms);
  }

  filter(val: string): any {
    return this.listSymptoms.filter(symptom =>
      symptom.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  private _getAllSymptoms() {
    this._aporteServices.fnGetAllSymptoms().subscribe(
      this._setAllSymptoms.bind(this),
      this._errorGetAllSymptoms.bind(this)
    );
  }
  private _setAllSymptoms(result: any) {
    this.listSymptoms = result['data'];
    this.fnInitFilter();
  }
  private _errorGetAllSymptoms(result: any) {
    console.log(result);
  }

  private getAllrelationships() {
    this._aporteServices.fnGetAllrelationships().subscribe(
      this._setAllRelationships.bind(this),
      this._errorGetAllRelationships.bind(this)
    );
  }
  private _setAllRelationships(result: any) {
    this.objAllrelationships = result['data'];
  }
  private _errorGetAllRelationships(error: any) {
    console.log('este es el error: ' + error.message + ' ..en: _errorGetAllRelationships' );
  }

  remove(simptom: any): void {
    let index = this.simptoms.indexOf(simptom);

    if (index >= 0) {
      this.simptoms.splice(index, 1);

      if (this.simptoms.length <= 0) {
        this.thirdFormGroup.controls.arrSymptoms.reset('');
      }
    }
  }

}
