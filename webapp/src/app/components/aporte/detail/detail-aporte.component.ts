import { Component, OnInit } from '@angular/core';
import {AporteService} from '../../../services/aporte/aporte.service';
import {MapaService} from '../../../services/mapa/mapa.service';
import {UserService} from '../../../services/user/user.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-aporte',
  templateUrl: './detail-aporte.component.html',
  styleUrls: ['./detail-aporte.component.css'],
  providers: [AporteService]
})
export class DetailAporteComponent implements OnInit {

  private _idAporte: string;
  isLinear = false;
  disabledFields: boolean = true;

  selectable: boolean = true;

  symptoms = [];
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  public objAllrelationships: object;

  constructor(private _activatedRouter: ActivatedRoute,
              private _formBuilder: FormBuilder,
              private _aporteService: AporteService) {
  }


  ngOnInit() {
    this._idAporte = this._activatedRouter.snapshot.params['id'];

    this._createForm();
    this._getData();
  }
  private _createForm() {
    this.secondFormGroup = this._formBuilder.group({
      name: [{value: '', disabled: this.disabledFields }],
      last_names: [{value: '', disabled: this.disabledFields }],
      city: [{value: '', disabled: this.disabledFields }],
      date_born: [{value: '', disabled: this.disabledFields }],
      relationsShip: [{value: '', disabled: this.disabledFields }]
    });
    this.thirdFormGroup = this._formBuilder.group({
      arrSymptoms: ''
    });
  }

  private _getData() {
    this._getDataAporteById();
    this._getDataAporteCityById();
    this._getDataAporteRelationshipById();
    this._getDataAporteSymptomsById();
  }

  private _getDataAporteById() {
    this._aporteService.fnGetAporteById(this._idAporte).subscribe(
      this._successGetAporteById.bind(this),
      this._errorGetAporteById.bind(this)
    );
  }
  private _successGetAporteById(result: any) {
    this.secondFormGroup.controls.name.reset(result['data']['name_person']);
    this.secondFormGroup.controls.last_names.reset(result['data']['last_name_person']);
    this.secondFormGroup.controls.date_born.reset(result['data']['date_born']);
  }
  private _errorGetAporteById(error: any) {
    console.log(error);
  }

  private _getDataAporteCityById() {
    this._aporteService.fnGetAporteCityById(this._idAporte).subscribe(
      this._successGetAporteCityById.bind(this),
      this._errorGetAporteCityById.bind(this)
    );
  }
  private _successGetAporteCityById(result: any) {
    this.secondFormGroup.controls.city.reset(result['data']['name']);
  }
  private _errorGetAporteCityById(error: any) {
    console.log(error);
  }

  private _getDataAporteRelationshipById() {
    this._aporteService.fnGetAporteRelationshipById(this._idAporte).subscribe(
      this._successGetAporteRelationshipById.bind(this),
      this._errorGetAporteRelationshipById.bind(this)
    );
  }
  private _successGetAporteRelationshipById(result: any) {
    this.secondFormGroup.controls.relationsShip.reset(result['data']['name']);
  }
  private _errorGetAporteRelationshipById(error: any) {
    console.log(error);
  }

  private _getDataAporteSymptomsById() {
    this._aporteService.fnGetAporteSymptomsById(this._idAporte).subscribe(
      this._successGetAporteSymptomsById.bind(this),
      this._errorGetAporteSymptomsById.bind(this)
    );
  }
  private _successGetAporteSymptomsById(result: any) {
    this.symptoms = result['data'];
  }
  private _errorGetAporteSymptomsById(error: any) {
    console.log(error);
  }
}
