import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAporteComponent } from './detail-aporte.component';

describe('DetailAporteComponent', () => {
  let component: DetailAporteComponent;
  let fixture: ComponentFixture<DetailAporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailAporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
