import { Component, OnInit } from '@angular/core';
import { User } from '../../Models/User';
import { UserService } from '../../services/user/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthServices } from '../../services/auth/auth.service';
import { LoginService } from '../../services/login/login.service';

import { Router } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators} from '@angular/forms';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [ UserService, AuthServices ]
})
export class RegisterComponent implements OnInit {

  public objUser: User;
  public resultData: any;
  public registerForm: FormGroup;

  constructor(private _userService: UserService,
              private _auth: AuthServices,
              private _router: Router,
              private _formBuilder: FormBuilder,
              private _loginService: LoginService) {
    this.objUser = new User('', '', '', '', '', '' );
  }

  ngOnInit() {
    if (this._auth.existUser()) {
      this._router.navigateByUrl('home');
    }
    this.createForm();
  }

  public createForm() {
    this.registerForm = this._formBuilder.group({
      name: ['', Validators.required ],
      last_names: ['', Validators.required ],
      email: ['', [Validators.required, Validators.email] ],
      password: ['', Validators.required ],
      password_confirmation: ['', Validators.required ],
      date_born: ['', Validators.required ],
    });
  }

  onSubmit() {
    this.resultData = this._userService.addUser(this.registerForm.value).subscribe(
      this._successRegister.bind(this),
      this._errorRegister.bind(this));
  }

  private _successRegister(result: any) {
    this._loginService.login(this.registerForm.value.email, this.registerForm.value.password);
  }

  private _errorRegister(error: any) {
    if (error instanceof HttpErrorResponse) {
      this._errorResponse(error);
    } else {
      console.log(error);
    }
  }

  private _errorResponse(error: any) {
    if (error.status === 401) {
      console.log('no autorizado');
      this._auth.errorAuthorization();
    } else {
      console.log('otro error del response');
      console.log(error);
    }
  }
}
