import { Component, OnInit } from '@angular/core';
import { MapaService } from '../../services/mapa/mapa.service';
import { Router } from '@angular/router';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  providers: [ MapaService ]
})
export class MapComponent implements OnInit {
  public latCenterChia: number;
  public longCenterChia: number;

  public objAllPositions: any;

  constructor(private _mapService: MapaService,
              private _router: Router) {
    this.latCenterChia = 4.865314;
    this.longCenterChia = -74.050961;
  }

  ngOnInit() {
    this._fnGetAllPosition();
  }

  private _fnGetAllPosition() {
    this._mapService.getAllPositions().subscribe(
      this.addAllPositions.bind(this),
      this.errorGetAllPositions.bind(this));
  }
  public parseInt(num: string) {
    return parseFloat(num);
  }

  addAllPositions(result: any) {
    this.objAllPositions = result['data'];
  }

  errorGetAllPositions(error) {
    console.log(error);
  }

}
