import { Component, OnInit } from '@angular/core';
import { AuthServices } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { Global } from '../../global';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  providers: [UserService]
})
export class MainComponent implements OnInit {
  public user: boolean;
  public url: string;

  constructor(private _auth: AuthServices,
              private _router: Router,
              private _global: Global,
              private _userService: UserService) {
    this.user = false;
    this.url = this._global.urlApi;
  }

  ngOnInit() {
    //TODO eliminar esto a la salida a production
    //localStorage.clear();
    this._auth.generateTokenAuthorization();
    this.user = this._auth.existUser();
  }

  public goToMisAportes() {
    if (this._auth.existUser()) {
      this._userService.getMyInformation().subscribe(
        this._successGetMyInformation.bind(this),
        this._errorGetMyInformation.bind(this)
      );
    }
  }
  private _successGetMyInformation(result: any) {
    let idUser: string;
    idUser = result['data']['id'];
    this._router.navigate(['my-aportes', idUser]);
  }
  private _errorGetMyInformation(error: any) {
    console.log('error en  _errorGetMyInformation');
  }

  public fnLogaout() {
    localStorage.clear();
    location.reload();
  }
  goToRealizarAporte() {
    console.log("ir a realizar aporte");
    this._router.navigateByUrl('realizar-aporte');
  }

  goToReportes() {
    console.log("ir a reportes");
  }
}
