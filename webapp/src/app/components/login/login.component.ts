import { Component, OnInit } from '@angular/core';
import { LoginService } from'../../services/login/login.service';
import { Router } from '@angular/router';
import { AuthServices } from '../../services/auth/auth.service';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators} from '@angular/forms';

import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'ng4-social-login';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(private _loginService: LoginService,
              private _router: Router,
              private _auth: AuthServices,
              private  _FormBuilder: FormBuilder
              /*private _authSocial: AuthService*/) {
  }

  ngOnInit() {
    if (this._auth.existUser()) {
      this._router.navigateByUrl('home');
    }
    this.createForm();
  }

  public createForm() {
    this.loginForm = this._FormBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  login() {
    this._loginService.login(this.loginForm.value.email, this.loginForm.value.password);
  }
  /*
  * for socila login
  * */
  /*
  loginFacebook(): void {
    this._authSocial.signIn(FacebookLoginProvider.PROVIDER_ID);
  }*/
  /*
  * loginSocial(socialPlatform: string) {
    let socialPlatformProvider: any;
    if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this._authSocial.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform + 'sign in data' + userData);
      }
    );
  }
  * */
}
