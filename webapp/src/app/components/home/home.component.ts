import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main/main.service';
import { Global } from '../../global';
@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [MainService]
})
export class HomeComponent implements OnInit {
  public userIsAdmin: boolean;
  public user: boolean;
  public url: string;

  constructor(private _mainService: MainService,
              private _global: Global) {
    this.userIsAdmin = false;
    this.user = false;
    this.url = this._global.urlApi;
  }

  ngOnInit() {
  }
}
