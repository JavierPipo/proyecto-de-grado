export class User {
  constructor(
    public name: string,
    public last_names: string,
    public email: string,
    public password: string,
    public password_confirmation: string,
    public date_born: string
  ) {}
}
