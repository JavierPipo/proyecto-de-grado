import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Global } from '../../global';

@Injectable()
export class MaladiesService {

  private _url: string;

  constructor(private _http: HttpClient,
              private _globals: Global) {
    this._url = this._globals.urlApi;
  }

  public getAllMaladies(): Observable<any> {
    return this._http.get(this._url + 'maladies');
  }

  public getOneMalady(idMalady) {
    return this._http.get(this._url + 'maladies/' + idMalady);
  }

  public getAllImagesByMaladyId(idMalady) {
    return this._http.get(this._url + 'maladies/' + idMalady + '/images');
  }

  public getAllSymptomsByMaladyId(idMalady) {
    return this._http.get(this._url + 'maladies/' + idMalady + '/symptoms');
  }
}
