import { TestBed, inject } from '@angular/core/testing';

import { MaladiesService } from './maladies.service';

describe('MaladiesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MaladiesService]
    });
  });

  it('should be created', inject([MaladiesService], (service: MaladiesService) => {
    expect(service).toBeTruthy();
  }));
});
