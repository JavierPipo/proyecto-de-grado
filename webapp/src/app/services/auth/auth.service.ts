import { Injectable } from '@angular/core';
import {Global} from '../../global';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthServices {

  private url: string;
  private client_id: number;
  private client_secret;

  private client_id_password: number;
  private client_secret_password: string;


  constructor(private global: Global,
              private http: HttpClient,
              private route: Router) {
    this.url = this.global.urlApi;
    this.client_id = this.global.client_id;
    this.client_secret = this.global.client_secret;

    this.client_id_password = this.global.client_id_passwort;
    this.client_secret_password = this.global.client_secret_passwort;
  }

  generateTokenAuthorization() {
    if (localStorage.getItem('Authorization') != null) {
    }else {
      this._generateClientToken();
    }
  }

  getTokenAuthorization(): string{
    return localStorage.getItem('Authorization');
  }

  existUser(): boolean {
    let result: boolean;
    result = localStorage.getItem('user') != null;
    return result;
  }

  exisClient(): boolean {
    let result: boolean;
    result = localStorage.getItem('client') != null;
    return result;
  }

  successLogin(objResult: any) {
    localStorage.clear();
    localStorage.setItem('user', '1');
    localStorage.setItem('Authorization', objResult['access_token']);
    localStorage.setItem('refresh_token', objResult['refresh_token']);
    location.reload();
  }

  errorLogin(err) {
    if (err instanceof HttpErrorResponse) {
      console.log(err.status);
    } else {
      console.log(err.message);
    }
  }

  getUserToken(user: string, password: string): Observable<any> {
    let objParams: object;

    objParams = {
      grant_type: 'password',
      client_id: this.client_id_password,
      client_secret: this.client_secret_password,
      username: user,
      password: password
    };
    console.log('hizo la peticion');
    return this.http.post(this.url + 'oauth/token', objParams);
  }

  private _generateRefreshToken(): Observable<any> {
    let objParams: object;

    objParams = {
      grant_type: 'refresh_token',
      client_id: this.client_id_password,
      client_secret: this.client_secret_password,
      refresh_token: this._getRefreshToken()
    };
    return this.http.post(this.url + 'oauth/token', objParams);
  }

  private _getRefreshToken(): string {
    return localStorage.getItem('refresh_token');
  }

  private _generateClientToken() {
    this._getClientAuthorization().subscribe(this._setClientToken.bind(this),
      this._errorTryGetClientAuthorization.bind(this));
  }

  private _existAuthorization(): boolean {
    let result: boolean;
    result = localStorage.getItem('Authorization') != null;
    return result;
  }

  _getClientAuthorization(): Observable<any> {
    let params: object;
    params = {
      grant_type: 'client_credentials',
      client_id: this.client_id,
      client_secret: this.client_secret
    };
    console.log('paso por el metodo de get Authoration client');
    return this.http.post(this.url + 'oauth/token', params);
  }

  private _setClientToken(objParams: object) {
    localStorage.setItem('client', '1');
    localStorage.setItem('Authorization', objParams['access_token']);
  }

  private _clearStorage(): void {
    localStorage.clear();
  }

  private _errorTryGetClientAuthorization(error: any) {
    if (error instanceof HttpErrorResponse) {
      console.log(error.message);
      console.log('error en esta peticion');
    } else {
      console.log('otro error en _errorTryGetClientAuthorization: ' + error.message);
    }
  }

  public errorAuthorization() {
    if (this.exisClient()) {
      this._generateClientToken();
    }else if (this.existUser()) {
      this._generateRefreshToken().subscribe(this._successRefreshToken.bind(this),
        this._errorRefreshToken.bind(this));
    }
  }

  private _successRefreshToken(result) {
    console.log('success refresh token');
    this.successLogin(result);
  }

  private _errorRefreshToken(error) {
    console.log('error refresh token');
  }
}
