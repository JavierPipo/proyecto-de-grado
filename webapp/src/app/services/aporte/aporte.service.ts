import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Global } from '../../global';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AporteService {

  private _url: string;

  constructor(private _http: HttpClient,
              private _global: Global) {
    this._url = this._global.urlApi;
  }

  public fnGetAllrelationships(): Observable<any> {
    return this._http.get(this._url + 'relationships');
  }

  public fnGetAllSymptoms(): Observable<any> {
    return this._http.get(this._url + 'symptoms');
  }

  public fnSaveAporte(objData: object, idUser: string): Observable<any> {
    return this._http.post(this._url + 'users/' + idUser + '/aportes', objData);
  }

  public fnGetAllMyAportes(_idUser: number): Observable<any> {
    return this._http.get(this._url + 'users/' + _idUser + '/aportes');
  }

  public fnGetAporteById(idAporte: string): Observable<any> {
    return this._http.get(this._url + 'aportes/' + idAporte);
  }
  public fnGetAporteCityById(idAporte: string): Observable<any> {
    return this._http.get(this._url + 'aportes/' + idAporte + '/cities');
  }
  public fnGetAporteSymptomsById(idAporte: string): Observable<any> {
    return this._http.get(this._url + 'aportes/' + idAporte + '/symptoms');
  }
  public fnGetAporteRelationshipById(idAporte: string): Observable<any> {
    return this._http.get(this._url + 'aportes/' + idAporte + '/relationships');
  }
}
