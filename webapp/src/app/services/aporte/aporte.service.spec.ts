import { TestBed, inject } from '@angular/core/testing';

import { AporteService } from './aporte.service';

describe('AporteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AporteService]
    });
  });

  it('should be created', inject([AporteService], (service: AporteService) => {
    expect(service).toBeTruthy();
  }));
});
