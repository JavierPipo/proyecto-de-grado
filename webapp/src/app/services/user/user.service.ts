import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Global } from '../../global';
import { User } from '../../Models/User';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {
  private url: string;
  constructor(private http: HttpClient,
              private globals: Global) {
    this.url = this.globals.urlApi;
  }

  addUser(userData: object): Observable<any> {
    //let params: string;
    console.log('user.service  agregar usuario');
   // params = JSON.stringify(userData);
    return this.http.post(this.url + 'users', userData);
  }

  public getMyInformation(): Observable<any> {
    return this.http.get(this.url + 'users/me');
  }
}
