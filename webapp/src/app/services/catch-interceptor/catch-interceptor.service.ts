import { Injectable, Injector } from '@angular/core';
import { AuthServices } from '../auth/auth.service';

import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpHeaderResponse,
  HttpProgressEvent,
  HttpSentEvent,
  HttpUserEvent, HttpHeaders, HttpErrorResponse, HttpEvent
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CatchInterceptorService implements HttpInterceptor {
  private _auth: any;
  constructor(private inj: Injector) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    this._auth = this.inj.get(AuthServices);

    let token: string;
    token = this._auth.getTokenAuthorization();

    let headers: any;
    if (!req.url.includes('/oauth/token')) {
      if (req.url.includes('https://maps.googleapis.com/maps/api/geocode')) {
      }else {
        headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
      }
    }
    const reqClone = req.clone({headers});

    return next.handle(reqClone);
  }
}
