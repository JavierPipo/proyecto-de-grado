import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Global } from '../../global';

@Injectable()
export class MapaService {

  private url: string;

  constructor(public http: HttpClient,
              private _global: Global) {
    this.url = this._global.urlApi;
  }

  getAllPositions(): Observable<any> {
    return this.http.get(this.url + 'aportes/positions');
  }

  public getDepartmentAndCityByLongLati(latitude: string, longitude: string): Observable<any> {
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&result_type=administrative_area_level_2&key=' + this._global.key_googleMaps);
  }
}
