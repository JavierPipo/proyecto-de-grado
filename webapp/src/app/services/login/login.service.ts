import {Injectable, Injector} from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { AuthServices } from '../auth/auth.service';
import { Router } from '@angular/router';

@Injectable()
export class LoginService {
  private _auth: any;
  constructor(public http: HttpClient,
              private injector: Injector,
              private _router: Router) {}

  login(strUserName: string, strPassword: string) {
    this._auth = this.injector.get(AuthServices);

    this._auth.getUserToken(strUserName, strPassword).subscribe(
      this._successLogin.bind(this),
      this._errorLogin.bind(this));
  }

  private _successLogin(result: any) {
    this._auth.successLogin(result);
  }
  private _errorLogin(error: any) {
    console.log('este es el login error');
    this._auth.errorLogin(error);
  }
}
