import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Global } from './global';

// routing
import { routing, appRoutingProviders } from './pp-routing/app.routing';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatButtonModule,
  MatCardModule,
  MatToolbarModule,
  MatTabsModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatListModule,
  MatMenuModule,
  MatSelectModule,
  MatOptionModule,
  MatStepperModule
} from '@angular/material';
import { HomeComponent } from './components/home/home.component';
import { MaladyListComponent } from './components/maladies/malady-list/malady-list.component';
import { MapComponent } from './components/map/map.component';
import { ErrorRouteComponent } from './components/error-route/error-route.component';
import { MaladyDetailComponent } from './components/maladies/malady-detail/malady-detail.component';
import { AgmCoreModule } from '@agm/core';
import { MainComponent } from './components/main/main.component';
import { LoginComponent } from './components/login/login.component';
import { MiPerfilComponent } from './components/mi-perfil/mi-perfil.component';
import { AporteComponent } from './components/aporte/mis aportes/aporte.component';
import { AcercaComponent } from './components/acerca/acerca.component';
import { ReportsComponent } from './components/reports/reports.component';
import { FooterComponent } from './components/footer/footer.component';
import { RegisterComponent } from './components/register/register.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {
  NgForm,
  NgModel,
  ReactiveFormsModule} from '@angular/forms';
import { CatchInterceptorService } from './services/catch-interceptor/catch-interceptor.service';
import { AuthServices } from './services/auth/auth.service';
import { CarouselModule } from 'ngx-bootstrap';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  LinkedinLoginProvider
} from 'ng4-social-login';
import { RealizarAporteComponent } from './components/aporte/realizar/realizar-aporte.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material';
import { MatSortModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { DetailAporteComponent } from './components/aporte/detail/detail-aporte.component';

/*const CONFIG = new AuthServiceConfig([
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('433327373780932'),
  }
]);

export function provideConfig() {
  return CONFIG;
}*/

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MaladyListComponent,
    MapComponent,
    ErrorRouteComponent,
    MaladyDetailComponent,
    MainComponent,
    LoginComponent,
    MiPerfilComponent,
    AporteComponent,
    AcercaComponent,
    ReportsComponent,
    FooterComponent,
    RegisterComponent,
    NgForm,
    NgModel,
    RealizarAporteComponent,
    DetailAporteComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatMenuModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatTabsModule,
    MatInputModule,
    MatAutocompleteModule,
    routing,
    MatChipsModule,
    MatDatepickerModule,
    AngularFontAwesomeModule,
    MatFormFieldModule,
    MatIconModule,
    HttpClientModule,
    MatListModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    SocialLoginModule,
    MatStepperModule,
    MatSelectModule,
    MatOptionModule,
    MatSortModule,
    MatTableModule,
    CarouselModule.forRoot(),
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyA-ac1SqhhgHy-Mhg-6_inoX2BMwONcha4'
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CatchInterceptorService,
      multi: true
    },/*
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },*/
    appRoutingProviders,
    DatePipe,
    Global,
    AuthServices
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
